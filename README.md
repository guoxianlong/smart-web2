功能介绍
=========
---------
**smart-web2** 是一套相对简单的OA系统；包含了流程设计器，表单设计器，表单列表管理，权限管理，简单报表管理等功能；
系统后端基于SpringMVC+Spring+Hibernate框架，前端页面采用JQuery+Bootstrap等主流技术；
流程引擎基于Snaker工作流；表单设计器基于雷劈网WEB表单设计器。
系统主要功能有：
 >1.系统管理
 >>系统管理包含有：基础信息管理、系统权限管理、版本管理、子系统管理。
 >
 >2.流程管理
 >>流程管理包含有：流程设计器、流程实例管理、流程页面模版管理等功能。
 >
 >3.表单管理
 >>表单管理包含有：表单设计器、表管理、表单帮助信息管理等。
 >
 >4.我的办公
 >>我的待办、我的已办;
 >
 >5.简单报表管理
 >>简单报表管理包含：简单报表的设计、报表管理等。
>
 >6.表单列表管理
 >> 通过表单设计器直接支持生成表单对应的列表及对应列表的管理（修改等）。

使用说明
=======
-------
---数据库MySQL5.6以上 <br/>
---下载后把data目录下的smart-web2_1.3.1.zip解压；然后解压出来的脚本文件（“smart-web2_1.3.1.sql”）导入到mysql数据库中；注：建库时，字符集编码为：utf8（utf8_general_ci）<br/>
---修改配置文件“jdbc.properties”，改成对应数据库的用户名和密码 <br/>
---“sysconfig.properties”系统配置文件；需要修改“root.dir”属性，设置为系统上传文件时用来存放的根目录 <br/>
----系统管理员用户名为：admin；密码为：123456 <br/>
----linux类系统需要修改mysql的配置文件，改为数据库表名不区分大小写（lower_case_table_names=1） <br />
环境要求
------------
1.jdk要求1.7及以上；<br />
2.tomcat6或tomcat7； <br />
3.eclipse版本4.4以上；<br />
4.浏览器要求：IE8及以上（最理想的是IE10及以上），火狐，chrome等。<br />

版本说明
----------
1.0.x 为第一个稳定版本（不含独立表单，表单需要和流程一起使用）<br />
1.1.x 为第二稳定版本（支持表单单独使用）<br />
1.2.x 为第三版本（简单报表管理）<br />
master 为开发版本（版本为1.3.1） <br />

**_本次更新功能亮点：新增表单列表的生成及管理_**<br />

版本更新说明
-------------
版本号：**1.3.1** <br/>
> 1.简单报表支持自定义单元格中的内容；
>
> 2.新增表单设计器支持生成表单列表功能；
>
> 3.表单生成的列表支持搜索及排序的功能；
>
> 4.表单设计器子表（列表控件）支持行统计的功能；
>
> 5.表单支持简单的编写JS脚本的功能（三个事件：加载时、表单提交前、表单提交后）；
>
> 6.删除数据支持逻辑删除（实体类需要继承LogicalDeleteSupport抽象类）；
>
> 7.大部分数据表删除数据时使用逻辑删除；
>
> 8.公网上无法下载的依赖jar包，改为本地依赖；
>
> 9.修复一些已知的问题。
>

**注意事项：**  
```text  
1.3.1版本新增了一些新功能；所以无法很好地兼容1.2.x的版本，升级到1.3.1后业务逻辑可能会改变，如：删除数据使用逻辑删除等。     

1.2.x升级到1.3.1需要修改某些表结构（在1.2.x版本中通过系统创建的数据库表，需要人为添加is_delete int(1)、 sort_order int(5)两个字段），否则运行表单实例会报错。

1.3.1版本开始所有ID长度都调整为50，所以使用老的数据表有可能会报插入的ID值太长的问题，需要人为的去修改ID的长度。
```

**_最后：全新的版本正在酝酿中，请敬请期待..._**

系统截图
=========
---------
 1.首页
![首页](https://git.oschina.net/bcworld/smart-web2/raw/master/screenshot/0001.png)
![修改密码](https://git.oschina.net/bcworld/smart-web2/raw/master/screenshot/0008.png)
 <br />
 2.资源管理 <br />
![资源管理列表](https://git.oschina.net/bcworld/smart-web2/raw/master/screenshot/0002.png)
![添加资源](https://git.oschina.net/bcworld/smart-web2/raw/master/screenshot/0009.png)
<br />
 3.菜单管理 <br />
![菜单管理列表](https://git.oschina.net/bcworld/smart-web2/raw/master/screenshot/0003.png)
![菜单管理列表](https://git.oschina.net/bcworld/smart-web2/raw/master/screenshot/0010.png)
 <br />
 4.权限管理
![权限管理列表](https://git.oschina.net/bcworld/smart-web2/raw/master/screenshot/0004.png)
 <br />
 5.流程管理
![流程设计器](https://git.oschina.net/bcworld/smart-web2/raw/master/screenshot/0005.png)
![流程实例管理](https://git.oschina.net/bcworld/smart-web2/raw/master/screenshot/0006.png)
 <br />
 6.表单管理
![表单设计器](https://git.oschina.net/bcworld/smart-web2/raw/master/screenshot/0007.png)


-------
技术交流
========
--------
QQ群：554013695
--------
版权声明
========
smart-web2使用[Apache v2 License](http://www.apache.org/licenses/LICENSE-2.0) 协议，请遵照协议内容.  

如果感觉对您有帮助，请作者喝杯咖啡吧o(￣︶￣)o  
<img src="https://gitee.com/bcworld/smart-web2/raw/master/screenshot/pay/zfb.jpg" width="300" height="450" /> <br />
<img src="https://gitee.com/bcworld/smart-web2/raw/master/screenshot/pay/weixin.png" width="300" height="450" />