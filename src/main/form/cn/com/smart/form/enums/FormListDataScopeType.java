package cn.com.smart.form.enums;

import java.util.ArrayList;
import java.util.List;

import com.mixsmart.utils.StringUtils;

import cn.com.smart.web.bean.LabelValue;

/**
 * 定义表单列表数据可见范围类型
 * @author lmq
 *
 */
public enum FormListDataScopeType {

    /**
     * 个人数据可见
     */
    PERSONAL("1", "个人数据可见", "personalAuth"),
    
    /**
     * 本部门数据可见
     */
    DEPT("2", "本部门数据可见", "deptAuth"),
    
    /**
     * 本部门及下级部门数据可见
     */
    DEPT_AND_SUB_DEPT("3", "本部门及下级部门数据可见", "deptSubDept"),
    
    /**
     * 授权部门数据可见
     */
    AUTH_DEPT("4", "授权部门数据可见", "authDeptAuth"),
    
    /**
     * 全部数据可见
     */
    ALL("5", "全部数据可见", "");
    
    private String value;
    
    private String text;

    private String authFlag;

    private FormListDataScopeType(String value, String text, String authFlag) {
        this.value = value;
        this.text = text;
        this.authFlag = authFlag;
    }

    /**
     * 获取数据可见范围类型对象
     * @param value 类型值
     * @return 返回类型对象
     */
    public static FormListDataScopeType getObj(String value) {
        if(StringUtils.isEmpty(value)) {
            return null;
        }
        FormListDataScopeType type = null;
        for(FormListDataScopeType typeTmp : FormListDataScopeType.values()) {
            if(typeTmp.getValue().equals(value)) {
                type = typeTmp;
                break;
            }
        }
        return type;
    }
    
    /**
     * 获取选项列表
     * @return 返回选项列表
     */
    public static List<LabelValue> getOptions() {
        List<LabelValue> labelValues = new ArrayList<LabelValue>();
        for(FormListDataScopeType typeTmp : FormListDataScopeType.values()) {
            labelValues.add(new LabelValue(typeTmp.getText(), typeTmp.getValue()));
        }
        return labelValues;
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuthFlag() {
        return authFlag;
    }

    public void setAuthFlag(String authFlag) {
        this.authFlag = authFlag;
    }
}
